package com.wmt.bansi;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {

    @GET("https://randomuser.me/api/?page=1&results=25")
    Call<JSONResponse> getJSON();
}
