package com.wmt.bansi;

import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("Photo")
    private String profilePhoto;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;

    @SerializedName("Email")
    private String email;

    @SerializedName("Date")
    private String dateOfBirth;

    public String getFirstName() {
        return firstName;
    }


    public String getProfilePhoto() {
        return profilePhoto;
    }


    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }
}
