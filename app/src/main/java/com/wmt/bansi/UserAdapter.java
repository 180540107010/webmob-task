package com.wmt.bansi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private ArrayList<UserModel> userList ;
    Context context;
    public UserAdapter(ArrayList<UserModel> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_userlist_details, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      //  holder.tvName.setText(userList.get(position).getFirstName() + " " + userList.get(position).getLastName());
        holder.tvEmail.setText(userList.get(position).getEmail());
        holder.tvDateOfBirth.setText(userList.get(position).getDateOfBirth());
        Picasso.get().load(userList.get(position).getProfilePhoto()).into(holder.ivProfilePhoto);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cvDetails;
        public TextView tvName,tvEmail,tvDateOfBirth;
        public ImageView ivProfilePhoto;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cvDetails = itemView.findViewById(R.id.cvUserDetails);
            tvName = itemView.findViewById(R.id.tvName);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvDateOfBirth = itemView.findViewById(R.id.tvDateOfBirth);
            ivProfilePhoto =itemView.findViewById(R.id.ivProfilePhoto);
        }
    }
}
